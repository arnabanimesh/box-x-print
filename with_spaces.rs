macro_rules! align {
    ($x:expr) => (format!("{}"," ".repeat($x)));
    ($x:expr, $y:expr, $($z:expr),*) => (
        format!("{}{}{}"," ".repeat($x),$y,align!($($z),*))
    )
}

fn main() {
    let mut inp = String::new();
    std::io::stdin().read_line(&mut inp).unwrap();
    let inp: String = inp
        .chars()
        .fold(String::new(), |a, b| format!("{}{} ", a, b))
        .trim()
        .parse()
        .unwrap();
    println!("{}", inp);
    let inp: Vec<char> = inp.chars().collect();
    let len: usize = inp.len();
    let mid: usize = len >> 1;
    let mut k: usize = 1;
    for i in (2..len - 1).step_by(2) {
        print!("{}", inp[i]);
        if (i == mid) && (len & 1 == 1) {
            print!("{}", align!(mid - 1, inp[i], mid - 1));
        } else {
            if i >= mid {
                k -= 2;
            }
            print!(
                "{}",
                align!(k, inp[i], len - 2 * k - 4, inp[len - i - 1], k)
            );
            if i < mid {
                k += 2;
            }
        }
        println!("{}", inp[len - i - 1]);
    }
    if len > 1 {
        println!("{}", &inp.iter().rev().collect::<String>());
    }
}
