#![feature(iter_intersperse)]

fn main() {
    let mut inp = String::new();
    std::io::stdin().read_line(&mut inp).unwrap();
    let inp: Vec<String> = inp
        .trim()
        .chars()
        .intersperse(' ')
        .map(|a| a.to_string())
        .collect();
    println!("{}", inp.concat());
    let len: usize = inp.len();
    let mid: usize = len >> 1;
    let mut k: usize = 1;
    for i in (2..len - 1).step_by(2) {
        print!("{}", inp[i]);
        if (i == mid) && (len & 1 == 1) {
            print!("{}", " ".repeat(mid - 1) + &inp[i] + &" ".repeat(mid - 1));
        } else {
            if i >= mid {
                k -= 2;
            }
            print!(
                "{}",
                " ".repeat(k)
                    + &inp[i]
                    + &" ".repeat(len - 2 * k - 4)
                    + &inp[len - i - 1]
                    + &" ".repeat(k)
            );
            if i < mid {
                k += 2;
            }
        }
        println!("{}", inp[len - i - 1]);
    }
    if len > 1 {
        println!(
            "{}",
            inp.iter().rev().map(|a| a.as_str()).collect::<String>()
        );
    }
}
