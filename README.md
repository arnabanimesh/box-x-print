# box-x-print

[Inspiration for box-x-print](https://www.facebook.com/groups/1412062792318164/permalink/1671508526373588/)

![image.png](./image.png)


### Example runs:
1. [Without spaces](https://www.ideone.com/zN0qpP)
2. [With spaces](https://www.ideone.com/sCcmwz)
